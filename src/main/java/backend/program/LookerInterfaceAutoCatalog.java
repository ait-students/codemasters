package backend.program;

import java.util.HashSet;

//Интерфейс с методами по управлению каталога автомобилей,
// к которым имеют доступ все пользователи от Looker до Director
public interface LookerInterfaceAutoCatalog {
    //метод просмотра всего списка автомобилей, что есть в нашем салоне
    public void displayAutoCatalog();
    //Метод поиска по марке (возвращает список машин определенной марки)
    public HashSet<Auto> searchAllAutosByBrand(String autoBrand);
    //Метод поиска по модели (возвращает список машин определенной модели)
    public HashSet<Auto> searchByModel(String autoModel);
    //Метод поиска машин по определенной ценовой категории от min до max
    //(возвращает список машин определенной ценовой категории)
    public HashSet<Auto> searchByPriceCategory(int minPrice, int maxPrice);
    //Метод поиска по году выпуска авто (возвращает список машин определенного года выпуска)
    public HashSet<Auto> searchByYear(int year);






}
