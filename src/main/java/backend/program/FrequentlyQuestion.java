package backend.program;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class FrequentlyQuestion {
    private static final Logger LOGGER = LoggerFactory.getLogger(Faq.class);
    private String answer;
    private String question;

    //Конструктор.
    public FrequentlyQuestion(String question, String answer) {
        if (question == null || question.isEmpty()) throw new IllegalArgumentException("Question cannot be null or empty");
        if (answer == null || answer.isEmpty()) throw new IllegalArgumentException("Answer cannot be null or empty");
        this.answer = answer;
        this.question = question;
    }


    //Геттеры, сеттеры.

    public String getAnswer() {
        return answer;
    }

    public void setAnswer(String answer) {

        if (answer == null || answer.isEmpty()) throw new IllegalArgumentException("Answer cannot be null or empty");
        this.answer = answer;
    }

    public String getQuestion() {
        return question;
    }

    public void setQuestion(String question) {
        if (question == null || question.isEmpty()) throw new IllegalArgumentException("Question cannot be null or empty");
        this.question = question;
    }

    @Override
    public String toString() {
        return "FrequentlyQuestion{" +
                "answer='" + answer + '\'' +
                ", question='" + question + '\'' +
                '}';
    }
}
