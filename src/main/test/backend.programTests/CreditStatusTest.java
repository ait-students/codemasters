package backend.programTests;

import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.EnumSource;

import static org.junit.jupiter.api.Assertions.assertNotNull;

public class CreditStatusTest {


    enum CreditStatus {  ACTIVE, OVERDUE, ARCHIVE;   }
    @ParameterizedTest
    @EnumSource(backend.program.CreditStatus.class)

    void testWithEnumSource(backend.program.CreditStatus creditStatus) {
        assertNotNull(creditStatus);
    }
}