package backend.programTests;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class PersonManagerTest {

    @Test
    void testGetUsersMethod() {
    }

    @Test
    void testGetEmployeesMethod() {
    }

    @Test
    void testSerializeUsersMethod() {
    }

    @Test
    void testDeserializeUsersMethod() {
    }

    @Test
    void testSerializeEmployeesMethod() {
    }

    @Test
    void testDeserializeEmployeesMethod() {
    }

    @Test
    void testAuthenticationMethodMethod() {
    }

    @Test
    void testAddUserMethod() {
    }

    @Test
    void testCreateUserMethod() {
    }

    @Test
    void testCreateEmployeeMethod() {
    }

    @Test
    void testAddEmployeeMethod() {
    }

    @Test
    void testFindUserByNameMethod() {
    }

    @Test
    void testFindUserByIDMethod() {
    }

    @Test
    void testFindEmployeeByIDMethod() {
    }

    @Test
    void testFindUserByEmailMethod() {
    }

    @Test
    void testFindEmployeeByEmailMethod() {
    }

    @Test
    void testRemoveUserByIdMethod() {
    }

    @Test
    void testRemoveEmployeeByIdMethod() {
    }

    @Test
    void testChangeEmployeeNameMethod() {
    }

    @Test
    void testChangeUserNameMethod() {
    }

    @Test
    void testChangeEmployeeEmailMethod() {
    }

    @Test
    void testChangeUserEmailMethod() {
    }

    @Test
    void testChangeEmployeePositionMethod() {
    }

    @Test
    void testChangeEmployeePhoneNumberMethod() {
    }

    @Test
    void testChangeUserPhoneNumberMethod() {
    }

    @Test
    void testChangeEmployeeAddressMethod() {
    }

    @Test
    void testChangeUserAddressMethod() {
    }

    @Test
    void testAddSaleToEmployeeByEmailMethod() {
    }

    @Test
    void testAddPurchaseToUserByEmailMethod() {
    }

    @Test
    void testAddTestDriveToUserByEmailMethod() {
    }

    @Test
    void testChangeEmployeeAccessLevelByEmailMethod() {
    }

    @Test
    void testChangeEmployeeAccessLevelByIdMethod() {
    }
}