package backend.programTests;

import backend.program.Auto;
import backend.program.Employee;
import java.time.LocalDate;
import java.util.HashMap;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class EmployeeTest {

    @Test
    void testGetDateOfHireMethod() {
        Employee employee = new Employee("Alex", "Manager", "223-32-23", "Germany", "hello@world.com", 18, "IT");
        LocalDate dateOfHire = employee.getDateOfHire();
        assertEquals(dateOfHire, employee.getDateOfHire());
    }

    @Test
    void testGetPositionMethod() {
        Employee employee = new Employee("Alex", "Manager", "223-32-23", "Germany", "hello@world.com", 18, "IT");
        String position = employee.getPosition();
        assertEquals(position, employee.getPosition());
    }

    @Test
    void testSetPositionMethod() {
        Employee employee = new Employee("Alex", "Manager", "223-32-23", "Germany", "hello@world.com", 18, "IT");
        employee.setPosition("Manager");
        assertEquals("Manager", employee.getPosition());
    }

    @Test
    void testGetSalesMethod() {
        HashMap<String, Auto> sales = new HashMap<String, Auto>();
        Employee employee = new Employee("Alex", "Manager", "223-32-23", "Germany", "hello@world.com", 18, "IT");
        assertEquals(sales, employee.getSales());
    }

    @Test
    void testAddSaleMethod() {
        HashMap<String, Auto> sales = new HashMap<String, Auto>();
        Employee employee = new Employee("Alex", "Manager", "223-32-23", "Germany", "hello@world.com", 18, "IT");
        Auto auto = new Auto("1234567890", "Audi", "A6", 100000, 2010, "быстрая", "красивая", "black");
        Auto previousAuto = sales.put(auto.getVinCode(), auto);
        if (previousAuto == null) {
            assertEquals("Audi", auto.getBrand(), "Added brand is Audi");
        }
    }
}
