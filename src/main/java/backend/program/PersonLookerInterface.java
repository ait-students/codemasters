package backend.program;

public interface PersonLookerInterface {

    //Метод для добавления пользователей в хранилище
    void addUser(User user);

    //Метод для создания пользователя
    void createUser (String name, String eMail, String phoneNumber, String address, String password);

}
