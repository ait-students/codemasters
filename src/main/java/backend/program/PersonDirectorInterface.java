package backend.program;

import java.util.ArrayList;

public interface PersonDirectorInterface extends PersonManagerInterface {

    //Метод для добавления сотрудников в хранилище
    void addEmployee(Employee employee);

    //Поиск пользователя по имени
    ArrayList<User> findUserByName(String name);

    //Поиск пользователя по id
    User findUserByID(int id);

    //Поиск сотрудника по id
    public Employee findEmployeeByID(int id);

    // Метод для удаления сотрудника по ID
    void removeEmployeeById(int id);

    //Метод для изменения имени сотрудника
    void changeEmployeeName(int id, String newName);

    //Метод для изменения E-mail сотрудника
    void changeEmployeeEmail(int id, String newEmail);

    //Метод для изменения должности сотрудника
    void changeEmployeePosition(int id, String newPosition);

    //Метод для изменения номера телефона сотрудника
    void changeEmployeePhoneNumber(int id, String newPhoneNumber);

    //Метод для изменения адреса сотрудника
    void changeEmployeeAddress(int id, String newAddress);

    //Создание профиля сотрудника
    void createEmployee (String name, String eMail, String phoneNumber, String address, String password, String position);
    //Метод для добавления продажи сотруднику
    public void addSaleToEmployee (Employee employee, Auto auto);

    // Поиск пользователя по e-mail
    public User findUserByEmail(String email);

    // Поиск сотрудника по e-mail
    public Employee findEmployeeByEmail(String email);
}

