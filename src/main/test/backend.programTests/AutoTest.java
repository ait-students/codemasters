package backend.programTests;

import backend.program.Auto;
import java.util.Date;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

class AutoTest {

    @Test
    void testGetVinCodeMethod() {
       Auto auto = new Auto("1234567890", "Audi", "A6", 100000, 2010, "быстрая", "красивая", "black");
       Assertions.assertEquals("1234567890", auto.getVinCode(),"VinCode should be 1234567890");
    }

    @Test
    void testGetBrandMethod() {
        Auto auto = new Auto("1234567890", "Audi", "A6", 100000, 2010, "быстрая", "красивая", "black");
        Assertions.assertEquals("Audi", auto.getBrand(),"Brand should be Audi");

    }

    @Test
    void testGgetModelMethod() {
        Auto auto = new Auto("1234567890", "Audi", "A6", 100000, 2010, "быстрая", "красивая", "black");
        Assertions.assertEquals("A6", auto.getModel(),"Model should be A6");
    }

    @Test
    void testGetPriceMethod() {
        Auto auto = new Auto("1234567890", "Audi", "A6", 100000, 2010, "быстрая", "красивая", "black");
        Assertions.assertEquals(100000, auto.getPrice(),"Price should be 100000");
    }

    @Test
    void testGetYearOfProductionMethod() {
        Auto auto = new Auto("1234567890", "Audi", "A6", 100000, 2010, "быстрая", "красивая", "black");
        Assertions.assertEquals("2010", auto.getYearOfProduction(),"Price should be 2010");
    }

    @Test
    void testGetShortCharacteristicsMethod() {
        Auto auto = new Auto("1234567890", "Audi", "A6", 100000, 2010, "быстрая", "красивая", "black");
        Assertions.assertEquals("быстрая", auto.getShortCharacteristics());

    }

    @Test
    void testGetFullCharacteristicsMethod() {
        Auto auto = new Auto("1234567890", "Audi", "A6", 100000, 2010, "быстрая", "красивая", "black");
        Assertions.assertEquals("красивая", auto.getFullCharacteristics());

    }

    @Test
    void testGgetColorMethod() {
        Auto auto = new Auto("1234567890", "Audi", "A6", 100000, 2010, "быстрая", "красивая", "black");
        Assertions.assertEquals("black", auto.getColor());
    }

    @Test
    void testGetDateMethod() {
        Auto auto = new Auto("1234567890", "Audi", "A6", 100000, 2010, "быстрая", "красивая", "black");
        Assertions.assertEquals(null, auto.getDate());
    }

    @Test
    void testSetVinCodeMethod() {
        Auto auto = new Auto("1234567890", "Audi", "A6", 100000, 2010, "быстрая", "красивая", "black");
        auto.setVinCode("2224567890");
        Assertions.assertEquals("2224567890", auto.getVinCode());

    }

    @Test
    void testSetBrandMethod() {
        Auto auto = new Auto("1234567890", "Audi", "A6", 100000, 2010, "быстрая", "красивая", "black");
        auto.setBrand("Mercedes-Benz");
        Assertions.assertEquals("Mercedes-Benz", auto.getBrand(),"Brand should be Mercedes-Benz");
    }

    @Test
    void testSetModelMethod() {
        Auto auto = new Auto("1234567890", "Audi", "A6", 100000, 2010, "быстрая", "красивая", "black");
        auto.setModel("EQS");
        Assertions.assertEquals("EQS", auto.getModel(),"Model should be EQS");
    }

    @Test
    void testSetPriceMethod() {
        Auto auto = new Auto("1234567890", "Audi", "A6", 100000, 2010, "быстрая", "красивая", "black");
        auto.setPrice(60000);
        Assertions.assertEquals(60000, auto.getPrice(),"Price should be 60000");
    }

    @Test
    void testSetYearOfProductionMethod() {
        Auto auto = new Auto("1234567890", "Audi", "A6", 100000, 2010, "быстрая", "красивая", "black");
        auto.setYearOfProduction(2024);
        Assertions.assertEquals("2024", auto.getYearOfProduction(),"YearOfProduction should be 2024");
    }

    @Test
    void testSetShortCharacteristicsMethod() {
        Auto auto = new Auto("1234567890", "Audi", "A6", 100000, 2010, "быстрая", "красивая", "black");
        auto.setShortCharacteristics("скоростная");
        Assertions.assertEquals("скоростная", auto.getShortCharacteristics(),"ShortCharacteristics should be скоростная");
    }

    @Test
    void testSetFullCharacteristicsMethod() {
        Auto auto = new Auto("1234567890", "Audi", "A6", 100000, 2010, "быстрая", "красивая", "black");
        auto.setFullCharacteristics("лучшая");
        Assertions.assertEquals("лучшая", auto.getFullCharacteristics(),"ShortCharacteristics should be скоростная");
    }

    @Test
    void testSetColorMethod() {
        Auto auto = new Auto("1234567890", "Audi", "A6", 100000, 2010, "быстрая", "красивая", "black");
        auto.setColor("blue");
        Assertions.assertEquals("blue", auto.getColor(),"Color should be blue");
    }

    @Test
    void testSetDateMethod() {
        Date date = new Date();
        date.setDate   (2024);
    }

}