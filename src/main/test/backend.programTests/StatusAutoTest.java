package backend.programTests;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.EnumSource;

import static org.junit.jupiter.api.Assertions.assertNotNull;

public class StatusAutoTest {
    enum StatusAuto {AUTOISFREEFORTESTDRIVE,AUTOISBUSYFORTESTDRIVE,AUTOISONSALE,AUTOWASSOLD};
    @ParameterizedTest
    @EnumSource(backend.program.StatusAuto.class)

    void testWithEnumSource(backend.program.StatusAuto statusAutoTest) {
        assertNotNull(statusAutoTest);
    }
}

