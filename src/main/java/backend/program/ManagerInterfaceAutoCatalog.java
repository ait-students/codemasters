package backend.program;

//Интерфейс с методами по управлению каталога автомобилей,
// к которым имеют доступ Manager и Director
public interface ManagerInterfaceAutoCatalog extends LookerInterfaceAutoCatalog{
    //метод добавления нового автомобиля в наш каталог
    public void addNewAutoInCatalog(Auto auto);
    //Метод удаления автомобиля из каталога
    public void removeAutoFromCatalog(Auto auto);
    //редактирование цены машины
    public void editingCarPrice(String vinCode, int newPrice);
    //редактирование краткой инфы о машине
    public void editingCarShortCharacteristics(String vinCode, String newShortCharacteristics);
    //Редактирование полной инфы о машине
    public void edidtingCarFullCharacteristics(String vinCode, String newFullCharacteristics);


}
