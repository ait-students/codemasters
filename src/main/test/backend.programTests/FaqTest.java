package backend.programTests;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.util.TreeMap;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class FaqTest {
    @Test
    void testAddFaqMethod() {
        TreeMap<String, String> faq = new TreeMap<>();

        Assertions.assertDoesNotThrow(() -> {
            faq.put("Question 1", "Answer 1");
        });
    }

    @Test
    void testDeleteFaqMethod() {
        TreeMap<String, String> faq = new TreeMap<>();

        Assertions.assertDoesNotThrow(() -> {
            faq.remove("Question 1");
        });
    }

    @Test
    void testSearchFaqMethod() {
        TreeMap<String, String> faq = new TreeMap<>();

        Assertions.assertDoesNotThrow(() -> {
            faq.get("Question 1");
        });
    }

    @Test
    void testShowAllFaqMethod() {
        TreeMap<String, String> faq = new TreeMap<>();

        Assertions.assertDoesNotThrow(() -> {
            faq.entrySet();
        });
    }

    @Test
    void testSerializeFaqMethod() throws IOException {
        TreeMap<String, String> faq = new TreeMap<>();
        ObjectOutputStream objectOutputStream = new ObjectOutputStream(new FileOutputStream("src/main/resources/faq")); {
            objectOutputStream.writeObject(faq);
            Assertions.assertEquals(0, faq.size());
    }
    }

    @Test
    void testDeserializeFaqMethod() throws IOException, ClassNotFoundException {
        Assertions.assertDoesNotThrow(() -> {

        });
    }

    @Test
    void testAddNewFaqMethod() {
        Assertions.assertDoesNotThrow(() -> {

        });

    }

    @Test
    void testParseFaqMethod() {
        Assertions.assertDoesNotThrow(() -> {

        });
    }

    @Test
    void testGetFaqMethod() {
        Assertions.assertDoesNotThrow(() -> {

        });
    }
}

