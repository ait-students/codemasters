package backend.programTests;

import backend.program.Person;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class MessageTest {

    @Test
    void testSetSenderMethod() {
        Person sender = new Person("Petr", "email", "34442", "Berlin", "email", 124);
        Assertions.assertEquals(sender, new Person("Petr", "email", "34442", "Berlin", "email", 124));
    }

    @Test
    void testGetSenderMethod() {

    }

    @Test
    void testGetReceiverMethod() {
    }

    @Test
    void testSetReceiverMethod() {
    }

    @Test
    void testGetTextMethod() {
    }

    @Test
    void testSetTextMethod() {
    }
}

