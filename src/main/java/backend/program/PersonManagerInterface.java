package backend.program;

import java.util.ArrayList;

public interface PersonManagerInterface {

    //этот интерфейс НЕ extends потому что ему не положены методы по изменению полей пользователя

    //Метод для добавления пользователей в хранилище
    void addUser(User user);

    void createUser (String name, String eMail, String phoneNumber, String address, String password);

    //Поиск пользователя по имени
    ArrayList<User> findUserByName(String name);

    //Поиск пользователя по id
    User findUserByID(int id);

    // Метод для удаления пользователя по ID
    void removeUserById(int id);

    //Метод для добавления тест - драйва пользователю
    public void addPurchaseToUser (User user, Auto auto);

    //Метод для добавления тест-драйва пользователю
    public void addTestDriveToUser (User user, TestDrive testDrive);

    // Поиск пользователя по e-mail
    public User findUserByEmail(String email);

    // Поиск сотрудника по e-mail
    public Employee findEmployeeByEmail(String email);
}
