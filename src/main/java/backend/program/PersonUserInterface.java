package backend.program;

public interface PersonUserInterface extends PersonLookerInterface {

    // Метод для удаления пользователя по ID
    void removeUserById(int id);

    //Метод для изменения имени пользователя
    void changeUserName(int id, String newName);

    //Метод для изменения E-mail пользователя
    void changeUserEmail(int id, String newEmail);

    //Метод для изменения номера телефона пользователя
    void changeUserPhoneNumber(int id, String newPhoneNumber);

    //Метод для изменения адреса пользователя
    void changeUserAddress(int id, String newAddress);

    //Метод для добавления тест - драйва пользователю
    public void addPurchaseToUser (User user, Auto auto);

    //Метод для добавления тест-драйва пользователю
    public void addTestDriveToUser (User user, TestDrive testDrive);
    }

