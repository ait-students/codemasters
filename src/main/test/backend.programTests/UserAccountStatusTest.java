package backend.programTests;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.EnumSource;

import static org.junit.jupiter.api.Assertions.*;

class UserAccountStatusTest {

    public enum UserAccountStatus {ACTIVE, SUSPENDED, DELETED;}

    @ParameterizedTest
    @EnumSource(backend.program.UserAccountStatus.class)

    void testWithEnumSource(backend.program.UserAccountStatus userAccountStatus) {
        assertNotNull(userAccountStatus);
    }

    @Test
    void getStatus() {
        assertEquals(UserAccountStatus.ACTIVE, UserAccountStatus.ACTIVE);
        assertEquals(UserAccountStatus.SUSPENDED, UserAccountStatus.SUSPENDED);
        assertEquals(UserAccountStatus.DELETED, UserAccountStatus.DELETED);
    }
}