package backend.programTests;

import backend.program.Auto;
import java.io.File;
import java.nio.file.Path;
import java.util.HashMap;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import static backend.program.AutoCatalog.getAutoCatalog;

public class AutoCatalogTest {

    @Test
    void testGetAutoCatalogMethod() {
        File autoCatalogTxt = Path.of("src", "main", "resources", "AutoCatalog.txt").toFile();
        File soldCarsTxt = Path.of("src", "main", "resources", "SoldCars.txt").toFile();
        Auto auto = new Auto("1234567890", "Audi", "A6", 100000, 2010, "быстрая", "красивая", "black");
        HashMap<String, Auto> getAutoCatalog= new HashMap<>(getAutoCatalog());
        Assertions.assertNotEquals(auto, getAutoCatalog()); 

    }

    @Test
    void testGetSoldCarsMethod() {
    }

    @Test
    void testRreadAutoCatalogFromFileMethod() {
    }

    @Test
    void testReadSoldCarsFromFileMethod() {
    }

    @Test
    void testWriteAutoCatalogToFileMethod() {
    }

    @Test
    void testWriteSoldAutoToFileMethod() {
    }

    @Test
    void testDisplayAutoCatalogMethod() {
    }

    @Test
    void testDisplaySoldAutoCatalogMethod() {
    }

    @Test
    void testSearchAllAutosByBrandMethod() {
    }

    @Test
    void testSearchByModelMethod() {
    }

    @Test
    void testSearchSoldByModelMethod() {
    }

    @Test
    void testSearchByPriceCategoryMethod() {
    }

    @Test
    void testSearchByYearMethod() {
    }

    @Test
    void testAddNewAutoInCatalogMethod() {
    }

    @Test
    void testRemoveAutoFromCatalogMethod() {
    }

    @Test
    void testEditingCarPriceMethod() {
    }

    @Test
    void testEditingCarShortCharacteristicsMethod() {
    }

    @Test
    void testEdidtingCarFullCharacteristicsMethod() {
    }

    @Test
    void testSearchAutoForSaleByVinCodeMethod() {
    }

    @Test
    void testSearchByVinCodeMethod() {
    }

    @Test
    void testGetAutosSoldBetweenDatesMethod() {
    }

    @Test
    void testCountAutosSoldByBrandBetweenDatesMethod() {
    }

    @Test
    void testBuyCarMethod() {
    }

}