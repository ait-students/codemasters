package backend.programTests;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.EnumSource;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

class AccessLevelTest {

    enum AccessLevel {  DIRECTOR,MANAGER,USER, NONE;   }
    @ParameterizedTest
    @EnumSource(backend.program.AccessLevel.class)

    void testWithEnumSource(backend.program.AccessLevel accessLevel) {
        assertNotNull(accessLevel);
    }

    @Test
    void test2WithEnumSource() {
        assertNotNull(AccessLevelTest.AccessLevel.DIRECTOR);
        assertNotNull(AccessLevelTest.AccessLevel.MANAGER);
        assertNotNull(AccessLevelTest.AccessLevel.USER);
        assertNotNull(AccessLevelTest.AccessLevel.NONE);
    }

    @Test
    void fullAccess() {
          assertEquals( backend.program.AccessLevel.DIRECTOR.level, 3, "Director will have Full access");
    }

    @Test
    void workspaceAccess() {
        assertEquals( backend.program.AccessLevel.MANAGER.level, 2, "Manager will have Full access");
    }

    @Test
    void clientspaceAccess() {
        assertEquals( backend.program.AccessLevel.USER.level, 1, "User will have Full access");
    }

    static class FaqTest {

        @Test
        void testAddFaqMethod() {
        }

        @Test
        void testDeleteFaqMethod() {
        }

        @Test
        void testSearchFaqMethod() {
        }

        @Test
        void testShowAllFaqMethod() {
        }

        @Test
        void testSerializeFaqMethod() {
        }

        @Test
        void testDeserializeFaqMethod() {
        }

        @Test
        void testAddNewFaqMethod() {
        }

        @Test
        void testParseFaqMethod() {
        }

        @Test
        void testGetFaqMethod() {
        }
    }
}